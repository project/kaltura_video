<?php

/**
 * @file
 * Contains kaltura_video.page.inc.
 *
 * Page callback for Kaltura Video entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Kaltura Video templates.
 *
 * Default template: kaltura_video.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_kaltura_video(array &$variables) {
  // Fetch KalturaVideo Entity Object.
  $kaltura_video = $variables['elements']['#kaltura_video'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

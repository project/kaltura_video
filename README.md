# Kaltura Video

A Drupal module which provides consolidated integration with Kaltura Video platform

## About Kaltura

Kaltura provides live and on-demand video SaaS solutions to thousands of organizations around the world, engaging hundreds of millions of viewers at home, at work, and at school. Their off-the-shelf SaaS products are known for their unparalleled flexibility, modularity, extendibility, and ease of integration. They are all built on top of hundreds of open APIs for video ingestion, transcoding, metadata management, distribution, publishing, engagement, monetization, and analytics.

## Module's Key Highlights
- Separate entity for Kaltura Video.
- Users can upload the video through the Drupal entity.
- Video can be synchronized in the drupal system through the cron job.
- Admin dashboard to check Katura Management console videos from drupal.
- Admin can import the single/multiple videos based on keyword search.
- Integrated with easy-to-use Kaltura PHP SDK.
- It's easy to integrate the video player in template.
- Users can edit the templates as per customer requirements.
- Zero usage of the drupal file system for videos.

## Important Links
- Kaltura API Doc: https://developer.kaltura.com/api-docs/Overview
- Kaltura Client libraries: https://developer.kaltura.com/api-docs/Client_Libraries
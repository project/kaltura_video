<?php

namespace Drupal\kaltura_video\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Class EmailNotifications.
 */
class KalturaVideoManagement {

  /**
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * KalturaVideoManagement constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
                              LoggerChannelFactoryInterface $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
  }

  /**
   * @param $video
   * @param bool $update
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function importVideos($video, $update = false) {

    if ($update) {
      $entity = $this->entityTypeManager->getStorage('kaltura_video')
        ->loadByProperties(['field_kaltura_video_id' => $video->id]);
      $entity = array_shift($entity);
      $entity->set('name', $video->name);
      $entity->set('field_description', $video->description);
      $entity->set('field_tags', $video->tags);
      $entity->set('field_thumbnail', $video->thumbnailUrl);
      $entity->save();
    }
    else {
      $entity = $this->entityTypeManager->getStorage('kaltura_video')
        ->create([
        'type' => 'video',
        'name' => $video->name,
        'field_description' => $video->description,
        'field_tags' => $video->tags,
        'field_kaltura_video_id' => $video->id,
        'field_thumbnail' => $video->thumbnailUrl
      ]);
      $entity->save();
      if ($entity->id()) {
        $this->logger->get('Kaltura Video :: Imported')->notice(t('Kaltura Video
       @id has been successfully imported', ['@id' => $video->id]));
      }
    }
  }

  /**
   * Check video exist or not.
   * @param $video_id
   *
   * @return \Drupal\Core\Entity\EntityInterface|mixed
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function isVideoExist($video_id) {
    $video = $this->entityTypeManager->getStorage('kaltura_video')
      ->loadByProperties(['field_kaltura_video_id' => $video_id]);
    return array_shift($video);
  }
}

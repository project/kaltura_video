<?php

namespace Drupal\kaltura_video;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\kaltura_video\Entity\KalturaVideoInterface;

/**
 * Defines the storage handler class for Kaltura Video entities.
 *
 * This extends the base storage class, adding required special handling for
 * Kaltura Video entities.
 *
 * @ingroup kaltura_video
 */
interface KalturaVideoStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Kaltura Video revision IDs for a specific Kaltura Video.
   *
   * @param \Drupal\kaltura_video\Entity\KalturaVideoInterface $entity
   *   The Kaltura Video entity.
   *
   * @return int[]
   *   Kaltura Video revision IDs (in ascending order).
   */
  public function revisionIds(KalturaVideoInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Kaltura Video author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Kaltura Video revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\kaltura_video\Entity\KalturaVideoInterface $entity
   *   The Kaltura Video entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(KalturaVideoInterface $entity);

  /**
   * Unsets the language for all Kaltura Video with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}

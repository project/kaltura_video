<?php

namespace Drupal\kaltura_video\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class KalturaVideoTypeForm.
 */
class KalturaVideoTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $kaltura_video_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $kaltura_video_type->label(),
      '#description' => $this->t("Label for the Kaltura Video type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $kaltura_video_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\kaltura_video\Entity\KalturaVideoType::load',
      ],
      '#disabled' => !$kaltura_video_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $kaltura_video_type = $this->entity;
    $status = $kaltura_video_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Kaltura Video type.', [
          '%label' => $kaltura_video_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Kaltura Video type.', [
          '%label' => $kaltura_video_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($kaltura_video_type->toUrl('collection'));
  }

}

<?php

namespace Drupal\kaltura_video\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Class KalturaVideoImport.
 *
 * @ingroup kaltura_video
 */
class KalturaVideoImport extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'kaltura_video_import';
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   * @throws \KalturaClientException
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $searchKey = \Drupal::request()->query->get('key');
    $header = $rows = [];
    $results = getKalturaVideoEntries($searchKey);
    if ($results) {
      $totalCount = $results->totalCount;

      if ($totalCount > 0) {
        $header = [
          'id' => $this->t('ID'),
          'thumb' => $this->t('Thumbnail'),
          'name' => $this->t('Name'),
          'created' => $this->t('Created On'),
          'updated' => $this->t('Update On'),
        ];

        foreach ($results->objects as $key => $video) {
          $rows[$video->id] = [
            'id' => $video->id,
            'thumb' => new FormattableMarkup('<img width="80" src="@src">', ['@src' => $video->thumbnailUrl]),
            'name' => $video->name,
            'created' => date("d M Y H:i s", $video->createdAt),
            'updated' => date("d M Y H:i s", $video->updatedAt)
          ];
          $_SESSION['kaltura_data'][$video->id] = $video;
        }
      }
    }

    $form['filter'] = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Filters'),
      '#open'  => true,
    ];

    $form['filter']['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search by free text'),
      '#default_value' => $searchKey,
      '#description' => $this->t('Search kaltura video by id, name, desc or tags')
    ];

    $form['filter']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
    ];

    $form['filter']['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
    ];

    $form['table'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $rows,
      '#empty' => t('No Videos Found!'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
      '#attributes' => [
        'class' => ['button--primary']
      ]
    ];

    $form['update'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update'),
    ];
    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \KalturaClientException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $op = (string)$form_state->getValue('op');
    if ($op === 'Search') {
      $field = $form_state->getValues();
      $key = $field["key"];
      $route_name = \Drupal::routeMatch()->getRouteName();
      $url = \Drupal\Core\Url::fromRoute($route_name)
        ->setRouteParameters(array('key'=> $key));
      $form_state->setRedirectUrl($url);
    }
    else if ($op === 'Reset') {
      $route_name = \Drupal::routeMatch()->getRouteName();
      $url = \Drupal\Core\Url::fromRoute($route_name);
      $form_state->setRedirectUrl($url);
    }
    else {
      $update = $op === 'Update' ? true : false;
      // Get Kaltura management service.
      $kalturaManagement = \Drupal::service('kaltura_video.kaltura_video_management');
      $videos = $form_state->getValue('table');
      foreach ($videos as $vid) {
        if (!empty($vid)) {
          $video = $kalturaManagement->isVideoExist($vid);
          if (!$video || $update) {
            $kaltura_data = $_SESSION['kaltura_data'][$vid];
            $kalturaManagement->importVideos($kaltura_data, $update);
            if ($update) {
              \Drupal::messenger()->addMessage('Video has been updated with Kaltura ID: ' . $vid);
            }
            else {
              \Drupal::messenger()->addMessage('Video has been imported with Kaltura ID: ' . $vid);
            }
          } else {
            \Drupal::messenger()->addWarning('Video already exist having Kaltura ID: ' . $vid);
          }
        }
      }
    }
  }
}

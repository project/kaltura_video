<?php

namespace Drupal\kaltura_video\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class VideoSearchForm.
 *
 * @ingroup kaltura_video
 */
class VideoSearchForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'kaltura_video_search_form';
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   * @throws \KalturaClientException
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $key = \Drupal::request()->query->get('key');

    $form['filters'] = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Filters'),
      '#open'  => true,
    ];

    $form['filters']['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search by free text'),
      '#default_value' => $key,
      '#description' => $this->t('Search kaltura video by id, name, desc or tags')
    ];

    $form['filters']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
    ];

    $form['filters']['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \KalturaClientException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $op = (string)$form_state->getValue('op');
    if ($op === 'Search') {
      $field = $form_state->getValues();
      $key = $field["key"];
      $route_name = \Drupal::routeMatch()->getRouteName();
      $url = \Drupal\Core\Url::fromRoute($route_name)
        ->setRouteParameters(array('key'=> $key));
      $form_state->setRedirectUrl($url);
    }
    else if ($op === 'Reset') {
      $route_name = \Drupal::routeMatch()->getRouteName();
      $url = \Drupal\Core\Url::fromRoute($route_name);
      $form_state->setRedirectUrl($url);
    }
  }

}

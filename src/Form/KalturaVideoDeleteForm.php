<?php

namespace Drupal\kaltura_video\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Kaltura Video entities.
 *
 * @ingroup kaltura_video
 */
class KalturaVideoDeleteForm extends ContentEntityDeleteForm {


}

<?php

namespace Drupal\kaltura_video\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class KalturaSettings.
 */
class KalturaSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'kaltura_video_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['kaltura_video.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('kaltura_video.settings');

    $form['kaltura'] = [
      '#type' => 'fieldset',
      '#title' => t('Kaltura Account Configurations'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['kaltura']['markup'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<p>You need to create account on KALTURA
        at <a href="https://corp.kaltura.com/">https://corp.kaltura.com/</a></p>'),
    ];
    $form['kaltura']['partner_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Partner ID'),
      '#default_value' => $config->get('kaltura.partner_id'),
      '#required' => TRUE,
      '#description' => $this->t('Copy the Partner ID from your Kaltura Management Console.
        This value can be found in Settings >> Account Info.')
    ];
    $form['kaltura']['admin_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Administrator Secret'),
      '#default_value' => $config->get('kaltura.admin_secret'),
      '#required' => TRUE,
      '#description' => $this->t('Copy the Admin Secret from your Kaltura Management Console.
        This value can be found in Settings >> Account Info.')
    ];
    $form['kaltura']['user_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Secret'),
      '#default_value' => $config->get('kaltura.user_secret'),
      '#required' => TRUE,
      '#description' => $this->t('Copy the User Secret from your Kaltura Management Console.
        This value can be found in Settings >> Account Info.')
    ];
    $form['kaltura']['user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Kaltura User'),
      '#default_value' => $config->get('kaltura.user'),
      '#required' => TRUE,
      '#description' => $this->t('Add user email ID used for Kaltura Account.')
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('kaltura_video.settings');
    $config->set('kaltura.partner_id', $form_state->getValue('partner_id'));
    $config->set('kaltura.admin_secret', $form_state->getValue('admin_secret'));
    $config->set('kaltura.user_secret', $form_state->getValue('user_secret'));
    $config->set('kaltura.user', $form_state->getValue('user'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}

<?php

namespace Drupal\kaltura_video;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\kaltura_video\Entity\KalturaVideoInterface;

/**
 * Defines the storage handler class for Kaltura Video entities.
 *
 * This extends the base storage class, adding required special handling for
 * Kaltura Video entities.
 *
 * @ingroup kaltura_video
 */
class KalturaVideoStorage extends SqlContentEntityStorage implements KalturaVideoStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(KalturaVideoInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {kaltura_video_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {kaltura_video_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(KalturaVideoInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {kaltura_video_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('kaltura_video_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}

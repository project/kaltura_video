<?php

namespace Drupal\kaltura_video\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Kaltura Video entities.
 *
 * @ingroup kaltura_video
 */
interface KalturaVideoInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Kaltura Video name.
   *
   * @return string
   *   Name of the Kaltura Video.
   */
  public function getName();

  /**
   * Sets the Kaltura Video name.
   *
   * @param string $name
   *   The Kaltura Video name.
   *
   * @return \Drupal\kaltura_video\Entity\KalturaVideoInterface
   *   The called Kaltura Video entity.
   */
  public function setName($name);

  /**
   * Gets the Kaltura Video creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Kaltura Video.
   */
  public function getCreatedTime();

  /**
   * Sets the Kaltura Video creation timestamp.
   *
   * @param int $timestamp
   *   The Kaltura Video creation timestamp.
   *
   * @return \Drupal\kaltura_video\Entity\KalturaVideoInterface
   *   The called Kaltura Video entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Kaltura Video revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Kaltura Video revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\kaltura_video\Entity\KalturaVideoInterface
   *   The called Kaltura Video entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Kaltura Video revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Kaltura Video revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\kaltura_video\Entity\KalturaVideoInterface
   *   The called Kaltura Video entity.
   */
  public function setRevisionUserId($uid);

}

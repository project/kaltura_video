<?php

namespace Drupal\kaltura_video\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Kaltura Video type entity.
 *
 * @ConfigEntityType(
 *   id = "kaltura_video_type",
 *   label = @Translation("Kaltura Video type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\kaltura_video\KalturaVideoTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\kaltura_video\Form\KalturaVideoTypeForm",
 *       "edit" = "Drupal\kaltura_video\Form\KalturaVideoTypeForm",
 *       "delete" = "Drupal\kaltura_video\Form\KalturaVideoTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\kaltura_video\KalturaVideoTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "kaltura_video_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "kaltura_video",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/kaltura_video_type/{kaltura_video_type}",
 *     "add-form" = "/admin/structure/kaltura_video_type/add",
 *     "edit-form" = "/admin/structure/kaltura_video_type/{kaltura_video_type}/edit",
 *     "delete-form" = "/admin/structure/kaltura_video_type/{kaltura_video_type}/delete",
 *     "collection" = "/admin/structure/kaltura_video_type"
 *   }
 * )
 */
class KalturaVideoType extends ConfigEntityBundleBase implements KalturaVideoTypeInterface {

  /**
   * The Kaltura Video type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Kaltura Video type label.
   *
   * @var string
   */
  protected $label;

}

<?php

namespace Drupal\kaltura_video\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Kaltura Video type entities.
 */
interface KalturaVideoTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}

<?php


namespace Drupal\kaltura_video\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Render\FormattableMarkup;


/**
 * Defines KalturaDashboard class.
 */
class KalturaDashboard extends ControllerBase {

  /**
   * Render KalturaDashboard callback
   */
  public function render() {
    //Get parameter value while submitting filter form
    $key = \Drupal::request()->query->get('key');
    //====load filter controller
    $form['search_form'] = $this->formBuilder()->getForm('Drupal\kaltura_video\Form\VideoSearchForm');
    $header = $rows = [];
    $summary = '';
    $results = getKalturaVideoEntries($key);
    if ($results) {
      $totalCount = $results->totalCount;
      if ($totalCount > 0) {
        $header[] = ['data' => $this->t('ID')];
        $header[] = ['data' => $this->t('Thumbnail')];
        $header[] = ['data' => $this->t('Name')];
        $header[] = ['data' => $this->t('Description')];
        $header[] = ['data' => $this->t('Tags')];
        $header[] = ['data' => $this->t('Plays')];
        $header[] = ['data' => $this->t('Created On')];
        $header[] = ['data' => $this->t('Update On')];

        foreach ($results->objects as $key => $video) {
          $rows[] = [
            $video->id,
            new FormattableMarkup('<img src="@src">', ['@src' => $video->thumbnailUrl]),
            $video->name,
            $video->description,
            $video->tags,
            $video->plays,
            date("d M Y H:i s", $video->createdAt),
            date("d M Y H:i s", $video->updatedAt),
          ];
        }
        $summary = "<p>Total Count: {$totalCount}</p>";
      }
    }

    $form['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No Videos Found!'),
      '#prefix' => $summary
    ];

    return $form;
  }

}

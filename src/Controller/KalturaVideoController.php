<?php

namespace Drupal\kaltura_video\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\kaltura_video\Entity\KalturaVideoInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class KalturaVideoController.
 *
 *  Returns responses for Kaltura Video routes.
 */
class KalturaVideoController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Kaltura Video revision.
   *
   * @param int $kaltura_video_revision
   *   The Kaltura Video revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($kaltura_video_revision) {
    $kaltura_video = $this->entityTypeManager()->getStorage('kaltura_video')
      ->loadRevision($kaltura_video_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('kaltura_video');

    return $view_builder->view($kaltura_video);
  }

  /**
   * Page title callback for a Kaltura Video revision.
   *
   * @param int $kaltura_video_revision
   *   The Kaltura Video revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($kaltura_video_revision) {
    $kaltura_video = $this->entityTypeManager()->getStorage('kaltura_video')
      ->loadRevision($kaltura_video_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $kaltura_video->label(),
      '%date' => $this->dateFormatter->format($kaltura_video->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Kaltura Video.
   *
   * @param \Drupal\kaltura_video\Entity\KalturaVideoInterface $kaltura_video
   *   A Kaltura Video object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(KalturaVideoInterface $kaltura_video) {
    $account = $this->currentUser();
    $kaltura_video_storage = $this->entityTypeManager()->getStorage('kaltura_video');

    $langcode = $kaltura_video->language()->getId();
    $langname = $kaltura_video->language()->getName();
    $languages = $kaltura_video->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $kaltura_video->label()]) : $this->t('Revisions for %title', ['%title' => $kaltura_video->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all kaltura video revisions") || $account->hasPermission('administer kaltura video entities')));
    $delete_permission = (($account->hasPermission("delete all kaltura video revisions") || $account->hasPermission('administer kaltura video entities')));

    $rows = [];

    $vids = $kaltura_video_storage->revisionIds($kaltura_video);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\kaltura_video\KalturaVideoInterface $revision */
      $revision = $kaltura_video_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $kaltura_video->getRevisionId()) {
          $link = $this->l($date, new Url('entity.kaltura_video.revision', [
            'kaltura_video' => $kaltura_video->id(),
            'kaltura_video_revision' => $vid,
          ]));
        }
        else {
          $link = $kaltura_video->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.kaltura_video.translation_revert', [
                'kaltura_video' => $kaltura_video->id(),
                'kaltura_video_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.kaltura_video.revision_revert', [
                'kaltura_video' => $kaltura_video->id(),
                'kaltura_video_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.kaltura_video.revision_delete', [
                'kaltura_video' => $kaltura_video->id(),
                'kaltura_video_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['kaltura_video_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}

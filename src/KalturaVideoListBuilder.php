<?php

namespace Drupal\kaltura_video;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Kaltura Video entities.
 *
 * @ingroup kaltura_video
 */
class KalturaVideoListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['kid'] = $this->t('Kaltura ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\kaltura_video\Entity\KalturaVideo $entity */
    $row['id'] = $entity->id();
    $kid = 'Broken Field';
    if ($entity->hasField('field_kaltura_video_id')) {
      $vid = $entity->get('field_kaltura_video_id')->getValue();
      $kid = $vid[0]['value'];
    }
    $row['kid'] = $kid;
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.kaltura_video.canonical',
      ['kaltura_video' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}

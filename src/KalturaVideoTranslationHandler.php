<?php

namespace Drupal\kaltura_video;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for kaltura_video.
 */
class KalturaVideoTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
